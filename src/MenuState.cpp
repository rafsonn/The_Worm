#include "MenuState.hpp"

#include <windows.h>

#include "GameState.hpp"
#include "OptionsState.hpp"


MenuState::MenuState(GameDataRef data) : _data(data)
{

}

void MenuState::Init()
{
    if(!background_t.loadFromFile(MENU_BACKGROUND_FILE_PATH))
    {
        MessageBox(NULL, "Missing texture file.", "ERROR", MB_ICONERROR);
        _data->window.close();
    }
    _background.setTexture(background_t);

    if(!logo_t.loadFromFile(MENU_LOGO_FILE_PATH))
    {
        MessageBox(NULL, "Missing texture file.", "ERROR", MB_ICONERROR);
        _data->window.close();
    }
    _logo.setTexture(logo_t);
    _logo.setOrigin(sf::Vector2f(_logo.getGlobalBounds().width / 2.0f, _logo.getGlobalBounds().height / 2.0f));
    _logo.setPosition(sf::Vector2f(WINDOW_WIDTH / 2.0f, 75.0f));

    std::string menuOptionsStrings[MENU_MAX_NUMBER_OF_BUTTONS] = { "PLAY", "OPTIONS", "EXIT" };

    for (int i = 0; i < MENU_MAX_NUMBER_OF_BUTTONS; i++)
    {
        _menu[i].setString(menuOptionsStrings[i]);
        _menu[i].setFont(_data->font);
        _menu[i].setCharacterSize(64);
        _menu[i].setColor(sf::Color::White);
        _menu[i].setOrigin(sf::Vector2f(_menu[i].getGlobalBounds().width / 2.0f, _menu[i].getGlobalBounds().height / 2.0f));
        _menu[i].setPosition(sf::Vector2f((WINDOW_WIDTH/2.0f), WINDOW_HEIGHT/(MENU_MAX_NUMBER_OF_BUTTONS+1)*(i+1)));
    }
    _menu[0].setColor(sf::Color::Cyan);

    _points.setFont(_data->font);
    _text.setFont(_data->font);
    _points.setFillColor(sf::Color::White);
    _text.setFillColor(sf::Color::White);
    _points.setCharacterSize(1.6*SCALE);
    _text.setCharacterSize(1.6*SCALE);
    _points.setString(intToString(loadingHighScore()));
    _text.setString("HIGH SCORE:");
    _points.setOrigin(_points.getGlobalBounds().width/2.0f, _points.getGlobalBounds().height/2.0f);
    _text.setOrigin(_text.getGlobalBounds().width/2.0f, _text.getGlobalBounds().height/2.0f);
    _points.setPosition(BOARD_X+BOARD_SIZE+140, 2.5*SCALE+40);
    _text.setPosition(BOARD_X+BOARD_SIZE+140, 2.5*SCALE);

}

void MenuState::ProcessEvent()
{
    while(_data->window.pollEvent(_data->event))
    {
        if(_data->event.type == sf::Event::Closed)
        {
            _data->window.close();
        }

        if(sf::Event::KeyReleased)
        {
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
                MoveUp();
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
                MoveDown();
            else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
            {
                switch(getSelected())
                {
                case 0:
                    std::cout<<"GAME"<<std::endl;
                    _data->engine.AddState(StateRef(new GameState(_data)), true);
                    break;
                case 1:
                    std::cout<<"OPTIONS"<<std::endl;
                    _data->engine.AddState(StateRef(new OptionsState(_data)), true);
                    break;
                case 2:
                    _data->window.close();
                    std::cout<<"EXIT"<<std::endl;
                    break;
                }
            }
        }
    }
}

void MenuState::Update()
{

}

void MenuState::Render()
{
    _data->window.clear(sf::Color::Cyan);

    _data->window.draw(_background);
    _data->window.draw(_logo);
    _data->window.draw(_text);
    _data->window.draw(_points);

    for(int i=0; i<MENU_MAX_NUMBER_OF_BUTTONS; i++)
        _data->window.draw(_menu[i]);

    _data->window.display();
}

void MenuState::MoveUp()
{
    if(selectedItem - 1 >= 0)
    {
        _menu[selectedItem].setFillColor(sf::Color::White);
        selectedItem--;
        _menu[selectedItem].setFillColor(sf::Color::Cyan);
    }
}

void MenuState::MoveDown()
{
    if(selectedItem + 1 < MENU_MAX_NUMBER_OF_BUTTONS)
    {
        _menu[selectedItem].setFillColor(sf::Color::White);
        selectedItem++;
        _menu[selectedItem].setFillColor(sf::Color::Cyan);
    }
}

int MenuState::getSelected()
{
    return selectedItem;
}

std::string MenuState::intToString(int number)
{
    std::stringstream text;

    text<<number;

    return text.str();
}

int MenuState::loadingHighScore()
{
    std::ifstream file;

    file.open("high_score.txt");
    file>>score;
    file.close();

    return score;
}

#include "Game.hpp"

#include <windows.h>

#include "LoadState.hpp"
#include"DEFINITIONS.hpp"


Game::Game(int width, int height, std::string title)
{
    _data->window.create(sf::VideoMode(width, height), title);
    _data->window.setFramerateLimit(FPS);

    _data->music.openFromFile(MENU_MUSIC_FILE_PATH);

    if(!_data->font.loadFromFile(FONT_FILE_PATH))
    {
        MessageBox(NULL, "Missing font file.", "ERROR", MB_ICONERROR);
        _data->window.close();
    }

    ///LOAD STATE
    _data->engine.AddState(StateRef(new LoadState(_data)));

    Run();
}

void Game::Run()
{
    _data->music.play();
    _data->music.setLoop(true);

    while(_data->window.isOpen())
    {
        _data->engine.ProcessStateChanges();

        _data->engine.GetActiveState()->ProcessEvent();
        _data->engine.GetActiveState()->Update();
        _data->engine.GetActiveState()->Render();
    }

    _data->music.~Music();
}
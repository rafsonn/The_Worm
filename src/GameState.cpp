#include "GameState.hpp"

#include <windows.h>

#include "GameOverState.hpp"
#include "DEFINITIONS.hpp"

GameState::GameState(GameDataRef data) : _data(data)
{

}

void GameState::Init()
{
    _hide.x = _hide.y = -1000;

    InitBackground();
    InitPoints();
    InitWorm();
    InitFood();
    InitBlockage();

    srand(time(NULL));
    _foodB.setPosition(pickLocation());
    AddSegment();
}

void GameState::ProcessEvent()
{
    while(_data->window.pollEvent(_data->event))
    {
        if(sf::Event::KeyPressed)
        {
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
            {
                if(dir!=d_RIGHT && dir == d_UP)
                {
                    std::cout<<"left"<<std::endl;
                    dir = d_LEFT;
                    _worm.rotate(-90);
                    for (int i=0; i<_worm_tail.size(); i++)
                    {
                        _worm_tail[i].rotate(-90);
                    }
                }
                else if(dir!=d_RIGHT && dir == d_DOWN)
                {
                    std::cout<<"left"<<std::endl;
                    dir = d_LEFT;
                    _worm.rotate(90);
                    for (int i=0; i<_worm_tail.size(); i++)
                    {
                        _worm_tail[i].rotate(90);
                    }
                }
            }

            else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
            {
                if(dir!=d_LEFT && dir == d_UP)
                {
                    std::cout<<"right"<<std::endl;
                    dir = d_RIGHT;
                    _worm.rotate(90);
                    for (int i=0; i<_worm_tail.size(); i++)
                    {
                        _worm_tail[i].rotate(90);
                    }
                }
                else if(dir!=d_LEFT && dir == d_DOWN)
                {
                    std::cout<<"right"<<std::endl;
                    dir = d_RIGHT;
                    _worm.rotate(-90);
                    for (int i=0; i<_worm_tail.size(); i++)
                    {
                        _worm_tail[i].rotate(-90);
                    }
                }
            }

            else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
            {
                if(dir!=d_DOWN && dir == d_LEFT)
                {
                    std::cout<<"up"<<std::endl;
                    dir = d_UP;
                    _worm.rotate(90);
                    for (int i=0; i<_worm_tail.size(); i++)
                    {
                        _worm_tail[i].rotate(90);
                    }
                }
                else if(dir!=d_DOWN && dir == d_RIGHT)
                {
                    std::cout<<"up"<<std::endl;
                    dir = d_UP;
                    _worm.rotate(-90);
                    for (int i=0; i<_worm_tail.size(); i++)
                    {
                        _worm_tail[i].rotate(-90);
                    }
                }
            }

            else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
            {
                if(dir!=d_UP && dir == d_LEFT)
                {
                    std::cout<<"down"<<std::endl;
                    dir = d_DOWN;
                    _worm.rotate(-90);
                    for (int i=0; i<_worm_tail.size(); i++)
                    {
                        _worm_tail[i].rotate(-90);
                    }
                }
                else if(dir!=d_UP && dir == d_RIGHT)
                {
                    std::cout<<"down"<<std::endl;
                    dir = d_DOWN;
                    _worm.rotate(90);
                    for (int i=0; i<_worm_tail.size(); i++)
                    {
                        _worm_tail[i].rotate(90);
                    }
                }
            }

            else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
            {
                _data->window.close();
            }
        }
    }
}

void GameState::Update()
{
    WormTail();
    Movement();
    Collision();
    FoodCollecting();
    FoodTime();
    Blockage();
    SavingHighScore(_counter);

    _points.setString(intToString(_counter));
    _points.setOrigin(_points.getGlobalBounds().width/2.0f, _points.getGlobalBounds().height/2.0f);
}

void GameState::Render()
{
    std::cout<<"x:"<<_worm_x<<" y:"<<_worm_y<<std::endl;

    _data->window.clear(sf::Color::Black);

    _data->window.draw(_board);
    _data->window.draw(_text);
    _data->window.draw(_points);
    _data->window.draw(_foodB);
    _data->window.draw(_foodG);
    _data->window.draw(_foodR);
    _data->window.draw(_worm);

    for (int j=0; j<_pile.size(); j++)
        _data->window.draw(_pile[j]);

    for (int i=0; i<_worm_tail.size(); i++)
        _data->window.draw(_worm_tail[i]);

    _data->window.display();
}

void GameState::InitBackground()
{
    if(!board_t.loadFromFile(BOARD_FILE_PATH))
    {
        MessageBox(NULL, "Missing texture file.", "ERROR", MB_ICONERROR);
        _data->window.close();
    }
    _board.setTexture(board_t);
}

void GameState::InitWorm()
{
    if(!worm_t.loadFromFile(WORM_HEAD_FILE_PATH))
    {
        MessageBox(NULL, "Missing texture file.", "ERROR", MB_ICONERROR);
        _data->window.close();
    }
    _worm.setTexture(worm_t);
    _worm.setOrigin(_worm.getGlobalBounds().width/2.0f, _worm.getGlobalBounds().height/2.0f);
    _worm.rotate(-90);

    if(!segment1_t.loadFromFile(WORM_SEGMENT1_FILE_PATH))
    {
        MessageBox(NULL, "Missing texture file.", "ERROR", MB_ICONERROR);
        _data->window.close();
    }

    if(!segment2_t.loadFromFile(WORM_SEGMENT2_FILE_PATH))
    {
        MessageBox(NULL, "Missing texture file.", "ERROR", MB_ICONERROR);
        _data->window.close();
    }

    if(!tail_t.loadFromFile(WORM_TAIL_FILE_PATH))
    {
        MessageBox(NULL, "Missing texture file.", "ERROR", MB_ICONERROR);
        _data->window.close();
    }

    _segment.setTexture(tail_t);
    _segment.setOrigin(_segment.getGlobalBounds().width/2.0f, _segment.getGlobalBounds().height/2.0f);
    _segment.rotate(90);
}

void GameState::InitFood()
{
    if(!foodB_t.loadFromFile(FOOD_B_FILE_PATH))
    {
        MessageBox(NULL, "Missing texture file.", "ERROR", MB_ICONERROR);
        _data->window.close();
    }
    _foodB.setTexture(foodB_t);
    _foodB.setOrigin(_foodB.getGlobalBounds().width/2.0f, _foodB.getGlobalBounds().width/2.0f);

    if(!foodG_t.loadFromFile(FOOD_G_FILE_PATH))
    {
        MessageBox(NULL, "Missing texture file.", "ERROR", MB_ICONERROR);
        _data->window.close();
    }
    _foodG.setTexture(foodG_t);
    _foodG.setOrigin(_foodG.getGlobalBounds().height/2.0f, _foodG.getGlobalBounds().width/2.0f);
    _foodG.setPosition(_hide);

    if(!foodR_t.loadFromFile(FOOD_R_FILE_PATH))
    {
        MessageBox(NULL, "Missing texture file.", "ERROR", MB_ICONERROR);
        _data->window.close();
    }
    _foodR.setTexture(foodR_t);
    _foodR.setOrigin(_foodR.getGlobalBounds().height/2.0f, _foodR.getGlobalBounds().width/2.0f);
    _foodR.setPosition(sf::Vector2f(_hide));
}

void GameState::InitBlockage()
{
    if(!stone_t.loadFromFile(BLOCKAGE_FILE_PATH))
    {
        MessageBox(NULL, "Missing texture file.", "ERROR", MB_ICONERROR);
        _data->window.close();
    }
    _stone.setTexture(stone_t);
    _stone.setOrigin(_stone.getGlobalBounds().width/2.0f, _stone.getGlobalBounds().height/2.0f);
    _stone.setPosition(_hide);
}

void GameState::InitPoints()
{
    _points.setFont(_data->font);
    _text.setFont(_data->font);
    _points.setFillColor(sf::Color::White);
    _text.setFillColor(sf::Color::White);
    _points.setCharacterSize(1.6*SCALE);
    _text.setCharacterSize(1.6*SCALE);
    _points.setString(intToString(_counter));
    _text.setString("POINTS:");
    _points.setOrigin(_points.getGlobalBounds().width/2.0f, _points.getGlobalBounds().height/2.0f);
    _text.setOrigin(_text.getGlobalBounds().width/2.0f, _text.getGlobalBounds().height/2.0f);
    _points.setPosition(BOARD_X+BOARD_SIZE+140, 2.5*SCALE+40);
    _text.setPosition(BOARD_X+BOARD_SIZE+140, 2.5*SCALE);
}

void GameState::Movement()
{
    switch(dir)
    {
    case d_LEFT:
        _worm_x-=SCALE;
        break;
    case d_RIGHT:
        _worm_x+=SCALE;
        break;
    case d_UP:
        _worm_y-=SCALE;
        break;
    case d_DOWN:
        _worm_y+=SCALE;
        break;
    }

    _worm.setPosition(_worm_x, _worm_y);
}

void GameState::WormTail()
{
    sf::Vector2f last_loc, temp;

    last_loc = _worm.getPosition();

    for (int i=0; i<_worm_tail.size(); i++)
    {
        temp = _worm_tail[i].getPosition();
        _worm_tail[i].setPosition(last_loc);
        last_loc = temp;
    }
}

void GameState::AddSegment()
{
    if(rot%2 == 0)
    {
        _worm_tail.insert(_worm_tail.begin(), _segment);
        _segment.setTexture(segment1_t);
    }

    else
    {
        _worm_tail.insert(_worm_tail.begin(), _segment);
        _segment.setTexture(segment2_t);
    }

    rot++;
}

sf::Vector2f GameState::pickLocation()
{
    sf::Vector2f position;

    position.x = rand()%36*SCALE+(BOARD_X+(SCALE/2));
    position.y = rand()%36*SCALE+(SCALE/2);

    return position;
}

void GameState::FoodCollecting()
{
    if(_worm.getGlobalBounds().intersects(_foodB.getGlobalBounds()))
    {
        _counter += 1;
        AddSegment();
        _foodB.setPosition(pickLocation());
        for(int i=0; i<_worm_tail.size(); i++)
        if(_foodB.getPosition() == _worm_tail[i].getPosition())
            _foodB.setPosition(pickLocation());

        for(int i=0; i<_pile.size(); i++)
        if(_foodB.getPosition() == _pile[i].getPosition())
            _foodB.setPosition(pickLocation());

        std::cout<<"collected\t"<<_counter<<std::endl;
    }

    if(_worm.getGlobalBounds().intersects(_foodG.getGlobalBounds()))
    {
        _counter += 5;
        AddSegment();
        _foodG.setPosition(sf::Vector2f(_hide));
        std::cout<<"collected\t"<<_counter<<std::endl;
    }

    if(_worm.getGlobalBounds().intersects(_foodR.getGlobalBounds()))
    {
        _counter += 10;
        AddSegment();
        _foodR.setPosition(sf::Vector2f(_hide));
        std::cout<<"collected\t"<<_counter<<std::endl;
    }
}

void GameState::FoodTime()
{
    _t_g = _clock_g.getElapsedTime();
    _t_r = _clock_r.getElapsedTime();

    if(_t_g.asSeconds() >= 20 && !_fG)
    {
        _foodG.setPosition(pickLocation());
        for(int i=0; i<_worm_tail.size(); i++)
        if(_foodG.getPosition() == _worm_tail[i].getPosition())
            _foodG.setPosition(pickLocation());

            for(int i=0; i<_pile.size(); i++)
        if(_foodG.getPosition() == _pile[i].getPosition())
            _foodG.setPosition(pickLocation());

        _fG = true;
    }
    if(_t_g.asSeconds() >= 25)
    {
        _fG = false;
        _foodG.setPosition(sf::Vector2f(_hide));
        _clock_g.restart();
    }

    if(_t_r.asSeconds() >= 35 && !_fR)
    {
        _foodR.setPosition(pickLocation());
        for(int i=0; i<_worm_tail.size(); i++)
        if(_foodR.getPosition() == _worm_tail[i].getPosition())
            _foodR.setPosition(pickLocation());

            for(int i=0; i<_pile.size(); i++)
        if(_foodR.getPosition() == _pile[i].getPosition())
            _foodR.setPosition(pickLocation());

        _fR = true;
    }
    if(_t_r.asSeconds() >= 40)
    {
        _fR = false;
        _foodR.setPosition(sf::Vector2f(_hide));
        _clock_r.restart();
    }
}

std::string GameState::intToString(int number)
{
    std::stringstream text;

    text<<number;

    return text.str();
}

void GameState::SavingHighScore(int score)
{
    std::fstream file;

    int temp = 0;

    file.open("high_score.txt", std::ios::in);
    file>>temp;
    file.close();

    if(score > temp)
    {
        file.open("high_score.txt", std::ios::out);
        file<<score;
        file.close();
    }
}

void GameState::Collision()
{
    if(_worm_x<=BOARD_X-(SCALE/2) || _worm_x>=BOARD_X+BOARD_SIZE || _worm_y<=BOARD_Y-(SCALE/2) || _worm_y>=BOARD_SIZE)
    {
        std::cout<<"collision"<<std::endl;
        _collide = true;
    }

    for(int i=0; i<_worm_tail.size(); i++)
        if(_worm.getGlobalBounds().intersects(_worm_tail[i].getGlobalBounds()))
        {
            std::cout<<"collision"<<std::endl;
            _collide = true;
        }

    for(int j=0; j<_pile.size(); j++)
        if(_worm.getGlobalBounds().intersects(_pile[j].getGlobalBounds()))
        {
            std::cout<<"collision"<<std::endl;
            _collide = true;
        }

    if(_collide)
    {
        ///SWITCH TO GAMEOVER
        _data->engine.AddState(StateRef(new GameOverState(_data)), false);
    }
}

void GameState::Blockage()
{
    _t_brick = _clock_brick.getElapsedTime();

    if(_t_brick.asSeconds() >= 10)
    {
        _stone.setPosition(sf::Vector2f(pickLocation()));
        _pile.push_back(_stone);
        _clock_brick.restart();
    }
}

#include "OptionsState.hpp"

#include <windows.h>

#include "GameState.hpp"
#include "MenuState.hpp"

OptionsState::OptionsState(GameDataRef data) : _data(data)
{

}

void OptionsState::Init()
{
    if(!background_t.loadFromFile(OPTIONS_BACKGROUND_FILE_PATH))
    {
        MessageBox(NULL, "Missing texture file.", "ERROR", MB_ICONERROR);
        _data->window.close();
    }
    _backgound.setTexture(background_t);

    std::string options[OPTIONS_MAX_NUMBER_OF_BUTTONS] = {"MUSIC ON/OFF", "MENU"};

    for(int i=0; i<OPTIONS_MAX_NUMBER_OF_BUTTONS; i++)
    {
        _options[i].setString(options[i]);
        _options[i].setFont(_data->font);
        _options[i].setCharacterSize(64);
        _options[i].setFillColor(sf::Color::White);
        _options[i].setOrigin(_options[i].getGlobalBounds().width/2.0f, _options[i].getGlobalBounds().height/2.0f);
        _options[i].setPosition(WINDOW_WIDTH/2.0f, WINDOW_HEIGHT/(MENU_MAX_NUMBER_OF_BUTTONS+1)*(i+1));
    }
    _options[0].setFillColor(sf::Color::Cyan);
}

void OptionsState::ProcessEvent()
{
    while(_data->window.pollEvent(_data->event))
    {
        if(_data->event.type == sf::Event::Closed)
        {
            _data->window.close();
        }

        if(_data->event.type == sf::Event::KeyPressed)
        {
            if(_data->event.key.code == sf::Keyboard::Up)
            {
                MoveUp();
            }

            if(_data->event.key.code == sf::Keyboard::Down)
            {
                MoveDown();
            }
        }

        if(_data->event.type == sf::Event::KeyPressed)
        {
            if(_data->event.key.code == sf::Keyboard::Return)
            {
                switch(getSelected())
                {
                case 0:
                    std::cout<<"MUSIC ON/OFF "<<std::endl;

                    ManageMusic();

                    break;
                case 1:
                    std::cout<<"MENU"<<std::endl;
                    _data->engine.AddState(StateRef(new MenuState(_data)), false);
                    break;
                }
            }
        }
    }
}

void OptionsState::Update()
{

}

void OptionsState::Render()
{
    _data->window.clear(sf::Color::Blue);

    _data->window.draw(_backgound);

    for(int i=0; i<OPTIONS_MAX_NUMBER_OF_BUTTONS; i++)
    _data->window.draw(_options[i]);

    _data->window.display();
}

void OptionsState::MoveUp()
{
    if(selectedItem - 1 >= 0)
    {
        _options[selectedItem].setFillColor(sf::Color::White);
        selectedItem--;
        _options[selectedItem].setFillColor(sf::Color::Cyan);
    }
}

void OptionsState::MoveDown()
{
    if(selectedItem + 1 < OPTIONS_MAX_NUMBER_OF_BUTTONS)
    {
        _options[selectedItem].setFillColor(sf::Color::White);
        selectedItem++;
        _options[selectedItem].setFillColor(sf::Color::Cyan);
    }
}

int OptionsState::getSelected()
{
    return selectedItem;
}

void OptionsState::ManageMusic()
{
    std::fstream file;

    bool Off = false;

    file.open("music_state.txt", std::ios::in);
    file>>Off;
    file.close();

    if(Off == false)
    {
        Off = true;
        _data->music.play();
    }

    else if (Off == true)
    {
        Off = false;
        _data->music.pause();
    }

    else
        Off = false;
    file.open("music_state.txt", std::ios::out);
    file<<Off;
    file.close();

    std::cout<<Off<<std::endl;
}

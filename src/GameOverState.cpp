#include "GameOverState.hpp"

#include <windows.h>

#include "MenuState.hpp"
#include "GameState.hpp"

GameOverState::GameOverState(GameDataRef data) : _data(data)
{

}

void GameOverState::Init()
{
    if(!lose_t.loadFromFile(GAME_OVER_BACKGROUND_FILE_PATH))
    {
        MessageBox(NULL, "Missing texture file.", "ERROR", MB_ICONERROR);
        _data->window.close();
    }
    _lose.setTexture(lose_t);

    _title.setString("GAME OVER");
    _title.setFont(_data->font);
    _title.setCharacterSize(86);
    _title.setColor(sf::Color::Red);
    _title.setOrigin(sf::Vector2f(_title.getGlobalBounds().width / 2.0f, _title.getGlobalBounds().height / 2.0f));
    _title.setPosition(sf::Vector2f(WINDOW_WIDTH / 2.0f, 50.f));

    std::string gameOver[GAME_OVER_NUMBER_OF_BUTTONS] = {"RETRY", "MENU"};

    for(int i=0; i<GAME_OVER_NUMBER_OF_BUTTONS; i++)
    {
        _gameOver[i].setFont(_data->font);
        _gameOver[i].setCharacterSize(64);
        _gameOver[i].setString(gameOver[i]);
        _gameOver[i].setOrigin(sf::Vector2f(_gameOver[i].getGlobalBounds().width/2.0f, _gameOver[i].getGlobalBounds().height/2.0f));
        _gameOver[i].setPosition(sf::Vector2f((WINDOW_WIDTH/2.0f), WINDOW_HEIGHT/(MENU_MAX_NUMBER_OF_BUTTONS+1)*(i+1)));
        _gameOver[i].setColor(sf::Color::White);
    }

    _gameOver[0].setFillColor(sf::Color::Cyan);
}

void GameOverState::ProcessEvent()
{
    while(_data->window.pollEvent(_data->event))
    {
        if(_data->event.type == sf::Event::Closed)
        {
            _data->window.close();
        }

        if(sf::Event::KeyPressed)
        {
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
            {
                MoveUp();
            }

            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
            {
               MoveDown();
            }

            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
            {
                switch(getSelected())
                {
                case 0:
                    std::cout<<"RETRY"<<std::endl;
                    _data->engine.AddState(StateRef(new GameState(_data)), true);
                    break;
                case 1:
                    std::cout<<"MENU"<<std::endl;
                    _data->engine.AddState(StateRef(new MenuState(_data)), true);
                    break;
                }
            }
        }
    }
}

void GameOverState::Update()
{

}

void GameOverState::Render()
{
    _data->window.clear(sf::Color::Red);

    _data->window.draw(_lose);
    _data->window.draw(_title);

    for(int i=0; i<GAME_OVER_NUMBER_OF_BUTTONS; i++)
    _data->window.draw(_gameOver[i]);

    _data->window.display();
}

void GameOverState::MoveUp()
{
    if(selectedItem - 1 >= 0)
    {
        _gameOver[selectedItem].setFillColor(sf::Color::White);
        selectedItem--;
        _gameOver[selectedItem].setFillColor(sf::Color::Cyan);
    }
}

void GameOverState::MoveDown()
{
    if(selectedItem + 1 < GAME_OVER_NUMBER_OF_BUTTONS)
    {
        _gameOver[selectedItem].setFillColor(sf::Color::White);
        selectedItem++;
        _gameOver[selectedItem].setFillColor(sf::Color::Cyan);
    }
}

int GameOverState::getSelected()
{
    return selectedItem;
}

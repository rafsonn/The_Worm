#include "Engine.hpp"

void Engine::AddState(StateRef newState, bool isReplacing)
{
    _isAdding = true;
    _isReplacing = isReplacing;

    _newState = std::move(newState);
}

void Engine::RemoveState()
{
    _isRemoving = true;
}

void Engine::ProcessStateChanges()
{
    if(_isRemoving && !_states.empty())
    {
        _states.pop();
        _isRemoving = false;
    }

    if(_isAdding)
    {
        if(!_states.empty())
        {
            if(_isReplacing)
            {
                _states.pop();
            }
        }

        _states.push(std::move(_newState));
        _states.top()->Init();
        _isAdding = false;
    }
}

StateRef &Engine::GetActiveState()
{
    return _states.top();
}

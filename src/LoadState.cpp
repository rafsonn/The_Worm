#include "LoadState.hpp"

#include <sstream>
#include <iostream>
#include <windows.h>

#include "DEFINITIONS.hpp"
#include "MenuState.hpp"


LoadState::LoadState(GameDataRef data) : _data(data)
{

}

void LoadState::Init()
{
    if(!loadScreen_t.loadFromFile(LOADING_SCREEN_FILE_PATH))
    {
        MessageBox(NULL, "Missing texture file.", "ERROR", MB_ICONERROR);
        _data->window.close();
    }
    _loadScreen.setTexture(loadScreen_t);
}

void LoadState::ProcessEvent()
{
    while(_data->window.pollEvent(_data->event))
    {
        if(sf::Event::Closed == _data->event.type)
        {
            _data->window.close();
        }
    }
}

void LoadState::Update()
{
    if(_clock.getElapsedTime().asSeconds() > LOADING_TIME)
    {
        ///SWITCH TO MENU
        _data->engine.AddState(StateRef(new MenuState(_data)));
    }
}

void LoadState::Render()
{
    _data->window.clear(sf::Color::Red);

    _data->window.draw(_loadScreen);

    _data->window.display();
}

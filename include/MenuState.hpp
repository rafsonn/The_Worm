#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include "State.hpp"
#include "Game.hpp"
#include "DEFINITIONS.hpp"

#include <iostream>
#include <sstream>
#include <fstream>


class MenuState : public State
{
    public:
        MenuState(GameDataRef data);

        void Init();

        void ProcessEvent();
        void Update();
        void Render();

    private:
        GameDataRef _data;

        sf::Sprite _background;
        sf::Sprite _logo;

        sf::Texture background_t;
        sf::Texture logo_t;

        sf::Text _points;
        sf::Text _text;
        sf::Text _menu[MENU_MAX_NUMBER_OF_BUTTONS];

        int selectedItem = 0;
        int score = 0;

        void MoveUp();
        void MoveDown();
        int getSelected();
        std::string intToString(int);
        int loadingHighScore();
};

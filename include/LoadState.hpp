#pragma once

#include <SFML/Graphics.hpp>

#include "State.hpp"
#include "Game.hpp"


class LoadState : public State
{
    public:
        LoadState(GameDataRef data);

        void Init();

        void ProcessEvent();
        void Update();
        void Render();

    private:
        GameDataRef _data;
        sf::Clock _clock;

        sf::Sprite _loadScreen;
        sf::Texture loadScreen_t;
};

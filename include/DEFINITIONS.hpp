#pragma once

#define WINDOW_WIDTH 1280
#define WINDOW_HEIGHT 720
#define FPS 10

#define SCALE 20

#define FONT_FILE_PATH "res/dpcomic.ttf"

#define LOADING_TIME 2
#define LOADING_SCREEN_FILE_PATH "res/loading_screen.png"

#define MENU_LOGO_FILE_PATH "res/logo.png"
#define MENU_BACKGROUND_FILE_PATH "res/background.png"
#define MENU_MUSIC_FILE_PATH "res/menu-edit.flac"
#define MENU_MAX_NUMBER_OF_BUTTONS 3

#define OPTIONS_BACKGROUND_FILE_PATH "res/background.png"
#define OPTIONS_MAX_NUMBER_OF_BUTTONS 2

#define GAME_OVER_BACKGROUND_FILE_PATH "res/background.png"
#define GAME_OVER_NUMBER_OF_BUTTONS 2

#define WORM_HEAD_FILE_PATH "res/head.png"
#define WORM_TAIL_FILE_PATH "res/tail.png"
#define WORM_SEGMENT1_FILE_PATH "res/segment1.png"
#define WORM_SEGMENT2_FILE_PATH "res/segment2.png"

#define FOOD_B_FILE_PATH "res/blueberry.png"
#define FOOD_G_FILE_PATH "res/watermelon.png"
#define FOOD_R_FILE_PATH "res/orange.png"

#define BLOCKAGE_FILE_PATH "res/stone.png"

#define BOARD_FILE_PATH "res/board.png"
#define BOARD_SIZE 720
#define BOARD_X 280
#define BOARD_Y 0


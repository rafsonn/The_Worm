#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <iostream>
#include <sstream>
#include <fstream>

#include "State.hpp"
#include "Game.hpp"
#include "DEFINITIONS.hpp"


class GameState : public State
{
    public:
        GameState(GameDataRef data);

        void Init();

        void ProcessEvent();
        void Update();
        void Render();

    private:
        GameDataRef _data;

        std::vector <sf::Sprite> _worm_tail;
        std::vector <sf::Sprite> _pile;

        sf::Sprite _worm;
        sf::Sprite _segment;
        sf::Sprite _board;
        sf::Sprite _foodB;
        sf::Sprite _foodG;
        sf::Sprite _foodR;
        sf::Sprite _stone;

        sf::Texture foodB_t;
        sf::Texture foodG_t;
        sf::Texture foodR_t;
        sf::Texture stone_t;
        sf::Texture worm_t;
        sf::Texture segment1_t;
        sf::Texture segment2_t;
        sf::Texture tail_t;
        sf::Texture board_t;

        sf::Clock _clock_g;
        sf::Clock _clock_r;
        sf::Clock _clock_brick;

        sf::Time _t_g;
        sf::Time _t_r;
        sf::Time _t_brick;

        sf::Vector2f _hide;
        sf::Text _points;
        sf::Text _text;


        void InitBackground();
        void InitWorm();
        void InitFood();
        void InitBlockage();
        void InitPoints();

        void Movement();
        sf::Vector2f pickLocation();
        void FoodCollecting();
        void FoodTime();
        void WormTail();
        void AddSegment();
        std::string intToString(int);
        void SavingHighScore(int);
        void Collision();
        void Blockage();

        float _worm_x = (WINDOW_WIDTH/2)+10;
        float _worm_y = (WINDOW_HEIGHT/2)+10;
        bool _collide = false;
        bool _fG = false;
        bool _fR = false;
        int _counter = 0;
        int rot = 0;

        enum direction
        {
            d_UP,
            d_DOWN,
            d_RIGHT,
            d_LEFT
        };
        direction dir = d_UP;
};

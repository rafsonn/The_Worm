#pragma once

#include <memory>
#include <string>
#include <fstream>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Engine.hpp"


struct GameData
{
    Engine engine;
    sf::RenderWindow window;
    sf::Event event;
    sf::Font font;
    sf::Music music;
};

typedef std::shared_ptr<GameData> GameDataRef;

class Game
{
    public:
        Game(int width, int height, std::string title);

    private:
        GameDataRef _data = std::make_shared<GameData>();

        void Run();
};



#pragma once

#include <SFML/Graphics.hpp>
#include <iostream>

#include "State.hpp"
#include "Game.hpp"
#include "DEFINITIONS.hpp"


class GameOverState : public State
{
public:
    GameOverState(GameDataRef data);

    void Init();

    void ProcessEvent();
    void Update();
    void Render();

private:
    GameDataRef _data;

    sf::Text _title;
    sf::Text _gameOver[GAME_OVER_NUMBER_OF_BUTTONS];

    sf::Sprite _lose;
    sf::Texture lose_t;

    int selectedItem = 0;

    void MoveUp();
    void MoveDown();
    int getSelected();

};


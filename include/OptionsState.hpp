#pragma once

#include <fstream>
#include <sstream>

#include "State.hpp"
#include "Game.hpp"
#include "DEFINITIONS.hpp"

class OptionsState : public State
{
    public:
        OptionsState(GameDataRef data);

        void Init();

        void ProcessEvent();
        void Update();
        void Render();

    private:
        GameDataRef _data;

        sf::Text _options[OPTIONS_MAX_NUMBER_OF_BUTTONS];
        sf::Sprite _backgound;
        sf::Texture background_t;

        int selectedItem = 0;

        void MoveUp();
        void MoveDown();
        int getSelected();
        void ManageMusic();
};

